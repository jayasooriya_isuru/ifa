# https://matplotlib.org/examples/color/named_colors.html

import collections
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors as mcolors

from matplotlib import rc
import matplotlib as mpl

def visualize():
    # https://stackoverflow.com/a/40781216
    # https://stackoverflow.com/a/44154062
    rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
    rc('text', usetex=True)

    color_scale = ['#FFCCCC','#FFDBC8','#FFECC8','#FFFDC8','#EEFDC8','#D4FDC8','#CCFDD1','#CCFDE2','#CCFDF2','#CCF4FB','#CCECFB',
                   '#CCDBFB','#CCCAFB','#DDCAFB','#EECAFB','#FFCAFB','#FFCAEA','#FFCAD9','#FFCAD0','#FF9999','#FFB996','#FFDB96',
                   '#FFFD96','#DDFD96','#AAFD96','#99FDA7','#99FDC9','#99FDEB','#99ECFC','#99DBFC','#99B9FC','#9997FC','#BB97FC',
                   '#DD97FC','#FF97FC','#FF97DA','#FF97B8','#FF97A7','#FF6666','#FF9864','#FFCB64','#FFFE64','#CCFE64','#7FFE64',
                   '#66FE7E','#66FEB1','#66FEE3','#66E4FD','#66CBFD','#6698FD','#6665FD','#9965FD','#CC65FD','#FF65FD','#FF65CA',
                   '#FF6597','#FF657D','#FF3333','#FF7632','#FFBA32','#FFFE32','#BBFE32','#55FE32','#33FE54','#33FE98','#33FEDC',
                   '#33DCFE','#33BAFE','#3376FE','#3332FE','#7732FE','#BB32FE','#FF32FE','#FF32BA','#FF3276','#FF3254','#FF0000',
                   '#FF5500','#FFAA00','#FFFF00','#AAFF00','#2AFF00','#00FF2B','#00FF80','#00FFD4','#00D4FF','#00AAFF','#0055FF',
                   '#0000FF','#5500FF','#AA00FF','#FF00FF','#FF00AA','#FF0055','#FF002A','#CC0000','#CC4400','#CC8800','#CCCC00',
                   '#88CC00','#22CC00','#00CC22','#00CC66','#00CCAA','#00AACC','#0088CC','#0044CC','#0000CC','#4400CC','#8800CC',
                   '#CC00CC','#CC0088','#CC0044','#CC0022','#990000','#993300','#996600','#999900','#669900','#199900','#00991A',
                   '#00994D','#00997F','#007F99','#006699','#003399','#000099','#330099','#660099','#990099','#990066','#990033',
                   '#990019','#660000','#662200','#664400','#666600','#446600','#116600','#006611','#006633','#006655','#005566',
                   '#004466','#002266','#000066','#220066','#440066','#660066','#660044','#660022','#660011','#000000','#0E0E0E',
                   '#1C1C1C','#2A2A2A','#393939','#474747','#555555','#636363','#717171','#7F7F7F','#8E8E8E','#9C9C9C','#AAAAAA',
                   '#B8B8B8','#C6C6C6','#D4D4D4','#E3E3E3','#F1F1F1','#FFFFFF']

    color_ratios = np.load('color_info.npy').flatten().tolist()

    colors = dict(list(zip(color_ratios, color_scale)))
    colors = {k: v for k, v in list(colors.items()) if k != 0}
    # https://stackoverflow.com/a/9001529
    colors = collections.OrderedDict(sorted(colors.items()))

    # descending order
    # https://stackoverflow.com/a/20101037
    color_ratios = list(colors.keys())[::-1]
    color_scale = list(colors.values())[::-1]

    n = len(colors)
    ncols = 5
    nrows = n // ncols + 1

    fig, ax = plt.subplots(figsize=(9, 9))
    fig.tight_layout()

    h = 40
    w = 40

    # reverse traverse to get descending order
    for i, color_ratio in enumerate(color_ratios):
        col = i % ncols
        row = i // ncols
        y = (row * h) + 20

        xi_line = w * (col + 0.22)
        xf_line = xi_line + 9
        xi_text = xi_line + 13
        
        # color name
        ax.text(xi_text, y-2, color_scale[i][1:], fontsize=8.5,
                horizontalalignment='left',
                verticalalignment='center')
        
        # color percentage
        # https://github.com/matplotlib/matplotlib/issues/4307/#issuecomment-89159778
        ax.text(xi_text, y+11, '{:.3f}\%'.format(color_ratio*100), fontsize=8.5,
                horizontalalignment='left',
                verticalalignment='center')    

        ax.hlines(y + h * 0.1, xi_line, xf_line,
                  color=color_scale[i], linewidth=30)

    ax.set_xlim(0, 200)
    ax.set_ylim(0, 600)
    ax.set_axis_off()

    fig.subplots_adjust(left=0, right=1, hspace=0, wspace=0)

    plt.gca().invert_yaxis()
    #plt.show()
    # https://stackoverflow.com/a/24534503
    plt.savefig("static/images/color_info.svg", format="svg")
    #fig.savefig("color_info.pdf", bbox_inches='tight', pad_inches=0)



    # https://stackoverflow.com/a/7082651
    mpl.rcParams['font.size'] = 6

    color_ratios_pie_chart_discrete = color_ratios[:15]
    color_ratios_pie_chart_other = sum(color_ratios[15:])
    color_ratios_pie_chart_discrete.append(color_ratios_pie_chart_other)

    color_scale_pie_chart_discrete = color_scale[:15]
    color_scale_pie_chart_other = '#999999'
    color_scale_pie_chart_discrete.append(color_scale_pie_chart_other)


    fig, ax = plt.subplots()
    ax.axis('equal')
    width = 0.3

    # https://stackoverflow.com/a/6170354
    def make_autopct(values):
        def my_autopct(pct):
            return '{:.1f}\%'.format(pct)
        return my_autopct

    pie, texts, _ = ax.pie(color_ratios_pie_chart_discrete, radius=0.5,
                    #labels=color_ratios_pie_chart_discrete,                
                    colors=color_scale_pie_chart_discrete,
                    counterclock=False,
                    startangle=90,
                    autopct=make_autopct(color_ratios_pie_chart_discrete),
                    pctdistance=1.15)
    #texts[0].set_fontsize(20)
    # https://stackoverflow.com/a/14279608
    pie[15].set_hatch('////')
    plt.setp( pie, width=width, edgecolor='white')

    # remove '#' from hex value
    legend_labels = [x[1:] for x in color_scale_pie_chart_discrete]
    legend_labels[-1] = 'Other'
    # https://stackoverflow.com/a/19858256
    # https://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.legend
    plt.legend(pie, legend_labels, loc='center right', fontsize=6, frameon=False)

    plt.savefig("static/images/color_pie.svg", format="svg")
    #fig.savefig("web_pie.pdf", bbox_inches='tight', pad_inches=0)


def main():
    visualize()

if __name__ == "__main__":
    main()