import os
import numpy as np
import cv2
from .wide_resnet import WideResNet


absolute_path = os.path.dirname(os.path.realpath(__file__))

def estimate_gender_age(img, top, bottom, left, right):
    '''
    Returns estimated gender and age from face detection bounding box.
    '''
    face = img[top:bottom, left:right]
    # get face detection shape to add boundaries
    face_height = bottom - top
    face_width = right - left
    img_height, img_width, _ = img.shape

    # add a reasonable boundary to face detection so that the 
    # gender, age estimator performs better
    new_top = max( 0, int(top-(face_height*0.6)) )
    new_bottom = min( img_height, int(bottom+(face_height*0.6)) )
    new_left = max( 0, int(left-(face_width*0.75)) )
    new_right = min( img_width, int(right+(face_width*0.75)) )

    # face detection with boundaries
    new_face = img[new_top:new_bottom, new_left:new_right]
    
    img_size = 64
    model = WideResNet(img_size, depth=16, k=8)()
    model.load_weights(os.path.join(absolute_path, "models", "weights.18-4.06.hdf5"))

    new_face_resized = cv2.resize(new_face, (64,64), interpolation = cv2.INTER_AREA)
    new_face_reshaped = new_face_resized.reshape((-1,64,64,3))

    # estimate gender and age
    results = model.predict(new_face_reshaped)
    predicted_gender = "Female" if results[0][0][0] > 0.5 else "Male"
    ages = np.arange(0, 101).reshape(101, 1)
    predicted_age = int(results[1].dot(ages).flatten()[0])
    
    return predicted_gender, predicted_age