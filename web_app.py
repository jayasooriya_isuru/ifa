from flask import Flask, render_template, request, redirect, url_for
from redis import Redis
import rq
import analysis
import nltk

nltk.download('vader_lexicon')

app = Flask(__name__, static_url_path='/static')
app.config['MONGO_URL'] = "mongodb://127.0.0.1:27017"
app.config['UPLOAD_PATH'] = "static/images/uploads"
app.config['JSON_PATH'] = "static/data/result_json.json"
app.config['RUNNING_ANALYSES_PATH'] = "static/data/running_analyses.json"
app.config['COMPLETED_ANALYSES_PATH'] = "static/data/completed_analyses.json"
app.config['STOP_WORDS_PATH'] = "static/data/stopwords.txt"


@app.route('/')
def main():
  return render_template('index.html')


@app.route('/running', methods = ['GET', 'POST'])
def get_running():
  # Get running analyses
  running_analyses = analysis.get_running(app.config['MONGO_URL'],
                                           app.config['RUNNING_ANALYSES_PATH'])
  return render_template('running.html')


@app.route('/completed', methods = ['GET', 'POST'])
def get_completed():
  # Get completed analyses
  completed_analyses = analysis.get_completed(app.config['MONGO_URL'],
                                           app.config['COMPLETED_ANALYSES_PATH'])
  return render_template('completed.html')

    
# @app.route('/report/<id>', methods = ['GET', 'POST'])
# def upload_file(id):


@app.route('/report/<id>', methods = ['GET', 'POST'])
def get_report(id):
  #if request.method == 'GET':
  return render_template('result.html', id=id)


@app.route('/add', methods = ['GET', 'POST'])
def add():

  if request.method == 'POST':
    # Get form values
    image_source = request.form['image-source']
    instagram_url = request.form['instagram-url']
    posts_count = int(request.form['posts-count'])

    #queue = rq.Queue(connection = Redis(host='127.0.0.1', port=6379))
    #job = queue.enqueue('analysis.run_analysis', instagram_url, app.config['MONGO_URL'],
                         #posts_count, app.config['UPLOAD_PATH'], app.config['STOP_WORDS_PATH'],
                         #app.config['JSON_PATH'])

    #analysis.run_analysis(instagram_url, app.config['MONGO_URL'],
                         #posts_count, app.config['UPLOAD_PATH'], app.config['STOP_WORDS_PATH'],
                         #app.config['JSON_PATH'])

    queue = rq.Queue('ifa-worker', connection = Redis(host='127.0.0.1', port=6379))
    job = queue.enqueue('analysis.run_analysis', instagram_url, app.config['MONGO_URL'],
                         posts_count, app.config['UPLOAD_PATH'], app.config['STOP_WORDS_PATH'],
                         app.config['JSON_PATH'])

    #return render_template('result.html')
    return redirect(url_for('get_running'))


if __name__ == '__main__':
  app.run(host='0.0.0.0')
