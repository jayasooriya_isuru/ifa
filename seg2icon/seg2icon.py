import os
import numpy as np
import cv2
import webcolors
import tensorflow as tf, sys
from sklearn.cluster import KMeans
from scipy import spatial
from PIL import Image, ImageDraw

absolute_path = os.path.dirname(os.path.realpath(__file__))


def sliding_window(image, step_size, window_size):
    # slide a window across the image
    for y in range(0, image.shape[0], step_size):
        for x in range(0, image.shape[1], step_size):
            # yield the current window
            yield (x, y, image[y:y + window_size, x:x + window_size])


def create_dress_patch(dress_segments_dir, dress_cat):
    '''
    Returns a square patch from non-black pixels of a segmented dress.
    '''
    seg_dress_file = os.path.join(dress_segments_dir, "%s.png" % dress_cat)
        
    ############### READ SEGMENTED DRESS ###############
    seg_dress = cv2.imread(seg_dress_file)
    seg_dress = cv2.cvtColor(seg_dress, cv2.COLOR_BGR2RGB)
    # create mask for findContours()
    # convert all non-black pixels to white
    seg_dress[np.where((seg_dress != [0,0,0]).all(axis = 2))] = [255,255,255]
    seg_dress = cv2.cvtColor(seg_dress, cv2.COLOR_RGB2GRAY)

    contours, _ = cv2.findContours(seg_dress, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # https://stackoverflow.com/a/17507192
    # find the index of the largest contour
    areas = [cv2.contourArea(c) for c in contours]
    max_index = np.argmax(areas)
    cnt = contours[max_index]

    x,y,w,h = cv2.boundingRect(cnt)
    #cv2.rectangle(seg_dress,(x,y),(x+w,y+h),(0,255,0),2)
    # crop image
    cropped_dress = cv2.imread(seg_dress_file)
    cropped_dress = cv2.cvtColor(cropped_dress, cv2.COLOR_BGR2RGB)
    cropped_dress = cropped_dress[(y):(y+h), (x):(x+w)]

    ################ CREATE IMAGE PATCH ################
    min_black_count = 4000 # greater than the pixel count of max window size (60*60)
    dress_patch = None

    # loop over different window sizes
    for window_size in [30,40,50,60]:
        # loop over the image
        for (x, y, window) in sliding_window(cropped_dress, 5, window_size):
            # if the window does not meet desired window size ignore it
            if window.shape[0] != window_size or window.shape[1] != window_size:
                continue
            # crop cropped_dress within the window
            window_img = cropped_dress[y:y + window_size, x:x + window_size]        
            # flatten pixel values from 2D to 1D
            # https://stackoverflow.com/a/953050
            pixel_list = [item for sublist in window_img.tolist() for item in sublist]
            # get no. of black pixels
            black_count = pixel_list.count([0,0,0])

            # create dress patch when there are minimum no. of background (black) pixels
            if black_count <= min_black_count:
                min_black_count = black_count
                dress_patch = window_img
                
    return dress_patch
  
# function to detect texture type from a square dress patch
def detect_texture(dress_patch):
    '''
    Returns pattern of a square dress patch
    '''
    _graph_file_path = os.path.join(absolute_path, "poets", "retrained_graph.pb")
    _label_file_path = os.path.join(absolute_path, "poets", "retrained_labels.txt")
    # unpersist graph from file
    with tf.gfile.FastGFile(_graph_file_path, "rb") as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
        _ = tf.import_graph_def(graph_def, name='')

    # loads label file, strips off carriage return
    label_lines = [line.rstrip() for line 
               in tf.gfile.GFile(_label_file_path)]

    with tf.Session() as sess:
        # feed dress patch image_data as input to the graph and get first prediction
        softmax_tensor = sess.graph.get_tensor_by_name("final_result:0")
        predictions = sess.run(softmax_tensor, {"DecodeJpeg:0": dress_patch})

        # sort to show labels of first prediction in order of confidence
        top_k = predictions[0].argsort()[-len(predictions[0]):][::-1]

        # TODO: put below in variables and return
        #for node_id in top_k:
        #    texture_label = label_lines[node_id]
        #    score = predictions[0][node_id]
        #    print('%s (score = %.5f)' % (texture_label, score))

    # assign texture type string
    if label_lines[top_k[0]] != 'other':
        texture = label_lines[top_k[0]]
    else:
        texture = ''
        
    return texture
  
# def find_nearest(array, value):
#     nearest_index = (np.abs(array - value)).argmin(axis = 0)
#     return array[nearest_index]

def find_nearest(array, value):
    # nearest_index = (np.abs(array - value)).argmin(axis = 0)
    # print(nearest_index)
    # print('#####')
    # return array[nearest_index,:]
    tree = spatial.cKDTree(array)
    mindist, minid = tree.query(value)
    return array[minid]

def centroid_histogram(clt):
    # grab the number of different clusters and create a histogram
    # based on the number of pixels assigned to each cluster
    num_labels = np.arange(0, len(np.unique(clt.labels_)) + 1)
    (hist, _) = np.histogram(clt.labels_, bins = num_labels)  
    # normalize the histogram, such that it sums to one
    hist = hist.astype("float")
    hist /= hist.sum()

    return hist    

def plot_colors(hist, centroids, image):
    # initialize the bar chart representing the relative frequency
    # of each of the colors
    bar = np.zeros((50, 300, 3), dtype = "uint8")
    # loop over the percentage of each cluster and the color of
    # each cluster
    bin_start = 0
    lab_colors = []
    for (percent, color) in zip(hist, centroids):
        # plot the relative percentage of each cluster
        color = find_nearest(image, color)
        bin_end = bin_start + (percent * 300)
        cv2.rectangle(bar, (int(bin_start), 0), (int(bin_end), 50),
            color.astype("uint8").tolist(), -1)
        # add closest Lab color to lab_colors list
        lab_colors.append(color)
        bin_start = bin_end
        
    lab_colors = np.array(lab_colors)    
    return bar, lab_colors

# https://stackoverflow.com/a/9694246
def closest_colour(requested_colour):
    min_colours = {}
    for key, name in list(webcolors.css3_hex_to_names.items()):
        r_c, g_c, b_c = webcolors.hex_to_rgb(key)
        rd = (r_c - requested_colour[0]) ** 2
        gd = (g_c - requested_colour[1]) ** 2
        bd = (b_c - requested_colour[2]) ** 2
        min_colours[(rd + gd + bd)] = name
    return min_colours[min(min_colours.keys())]

def get_colour_name(requested_colour):
    try:
        closest_name = actual_name = webcolors.rgb_to_name(requested_colour)
    except ValueError:
        closest_name = closest_colour(requested_colour)
        actual_name = None
    return actual_name, closest_name
  
def get_colors(dress_patch, mask_info=False, file = ""):
    ############### GET PROMINENT COLORS ###############
    if(mask_info == False):
        image = cv2.resize(dress_patch,(200,200))
        image = cv2.cvtColor(image, cv2.cv.CV_RGB2Lab)

        image = cv2.resize(image,(200,200))
        height, width, channels = image.shape

    # reshape the image to be a list of pixels
        image = image.reshape((height * width, 3))
    else:
        image  = dress_patch

    # cluster the pixel intensities
    clt = KMeans(n_clusters=3, random_state=1234)
    clt.fit(image)

    # build a histogram of clusters and then create a figure
    # representing the number of pixels labeled to each color
    hist = centroid_histogram(clt)
    bar, lab_colors = plot_colors(hist, clt.cluster_centers_, image)
    bar = cv2.cvtColor(bar, cv2.COLOR_Lab2RGB)
    if(mask_info == True):
        #cv2.imwrite(file,bar)
        np.save('%s.npy' % file, bar)

    ############## GET TOP 3 COLOR CODES ###############
    
    # Convert closest lab colors to cluster centers from Lab color space to RGB.
    # Dimensions are expanded to match cv2.cvtColor parameter and squeezed again
    # for the rest of the pipeline.
    lab_colors = np.expand_dims(lab_colors, axis=0)
    bgr_colors = cv2.cvtColor(lab_colors.astype(np.uint8), cv2.COLOR_Lab2RGB)
    rgb_colors = cv2.cvtColor(bgr_colors, cv2.COLOR_BGR2RGB)
    rgb_colors = np.squeeze(rgb_colors, axis=0)
    # sort hist and then get indexes of the values in their original order
    # https://stackoverflow.com/a/6422754
    indices = [i[0] for i in sorted(enumerate(hist), key=lambda x:x[1])]

    color_1 = rgb_colors[indices[0]]
    color_2 = rgb_colors[indices[1]]
    color_3 = rgb_colors[indices[2]] # highest percentage color

    requested_colour = (color_3[0], color_3[1], color_3[2])
    actual_name, closest_name = get_colour_name(requested_colour)
    dress_color = closest_name

    colors_hex = []    
    # https://stackoverflow.com/a/3380739
    colors_hex.append('#%02x%02x%02x' % (color_1[0], color_1[1], color_1[2]) )
    colors_hex.append('#%02x%02x%02x' % (color_2[0], color_2[1], color_2[2]) )
    colors_hex.append('#%02x%02x%02x' % (color_3[0], color_3[1], color_3[2]) )

    # create a list of color ratios in ascending order
    color_ratios = []
    color_ratios.append(hist[indices[0]])
    color_ratios.append(hist[indices[1]])
    color_ratios.append(hist[indices[2]])
    
    return colors_hex, color_ratios, bar, dress_color
  
def draw_pattern(colors_hex, dress_cat, texture):
    #################### DRAW PATCH ####################
    # http://pillow.readthedocs.io/en/3.1.x/reference/ImageDraw.html#example-draw-a-gray-cross-over-an-image
    # http://bradmontgomery.blogspot.com/2008/07/django-generating-image-with-pil.html
    # use most used color as background
    patch = Image.new('RGB', (50,50), colors_hex[2])
    draw = ImageDraw.Draw(patch)

    # draw basic shape based on texture
    if texture == "checked":
        # draw checked patch
        draw.line((0, patch.size[0]/2) + (patch.size[0], patch.size[0]/2),
                  fill=colors_hex[1], width=20)
        draw.line((patch.size[0]/2, 0, patch.size[0]/2, patch.size[0]),
                  fill=colors_hex[0], width=20)
    elif texture == "polka dotted":
        # draw dotted patch
        x = 25
        y = 25
        r = 10 # radius
        # draw circle
        draw.ellipse((x-r, y-r, x+r, y+r), fill=colors_hex[1])    
    elif texture == "striped":
        # draw striped patch
        draw.line((patch.size[0]/2, 0, patch.size[0]/2, patch.size[0]),
                  fill=colors_hex[1], width=20)
    else:
        # plain (do nothing)
        pass
    
    ################ DRAW PATTERN IMAGE ################
    # https://stackoverflow.com/a/24164270
    bg_w, bg_h = patch.size
    dress_icon_mask = Image.open(os.path.join(absolute_path, "icons", "%s_mask.jpg" % dress_cat))

    pattern = Image.new('RGB', dress_icon_mask.size)
    w, h = pattern.size

    # Iterate through a grid, to place the background tile
    for i in range(0, w, bg_w):
        for j in range(0, h, bg_h):
            # paste the image at location i, j:
            pattern.paste(patch, (i, j))
            
    return pattern
  
# https://stackoverflow.com/a/40899648
def blend_transparent(face_img, overlay_t_img):
    # Split out the transparency mask from the colour info
    overlay_img = overlay_t_img[:,:,:3] # Grab the BRG planes
    overlay_mask = overlay_t_img[:,:,3:]  # And the alpha plane

    # Again calculate the inverse mask
    background_mask = 255 - overlay_mask

    # Turn the masks into three channel, so we can use them as weights
    overlay_mask = cv2.cvtColor(overlay_mask, cv2.COLOR_GRAY2BGR)
    background_mask = cv2.cvtColor(background_mask, cv2.COLOR_GRAY2BGR)

    # Create a masked out face image, and masked out overlay
    # We convert the images to floating point in range 0.0 - 1.0
    face_part = (face_img * (1 / 255.0)) * (background_mask * (1 / 255.0))
    overlay_part = (overlay_img * (1 / 255.0)) * (overlay_mask * (1 / 255.0))

    # And finally just add them together, and rescale it back to an 8bit integer image    
    return np.uint8(cv2.addWeighted(face_part, 255.0, overlay_part, 255.0, 0.0))
  
def create_icon(pattern, dress_cat):
    ################ CREATE DRESS MASK #################
    dress_icon_mask = cv2.imread(os.path.join(absolute_path, "icons", "%s_mask.jpg" % dress_cat))

    th, im_th = cv2.threshold(dress_icon_mask, 127, 255, cv2.THRESH_BINARY);

    # invert colors for mask
    # http://docs.opencv.org/trunk/d7/d4d/tutorial_py_thresholding.html
    ret, thresholded = cv2.threshold(im_th,127,255,cv2.THRESH_BINARY_INV)

    # convert RGB to grayscale for mask
    gray_image = cv2.cvtColor(thresholded, cv2.COLOR_BGR2GRAY)

    # https://stackoverflow.com/a/25114860, https://stackoverflow.com/a/36509778
    pattern = np.asarray(pattern)
    # apply mask
    masked = cv2.bitwise_and(pattern, pattern, mask=gray_image)

    # https://stackoverflow.com/a/3169874
    # convert background from black to  white
    replacement_color = (255,255,255)
    orig_color = (0,0,0)

    pattern_dress = np.array(masked)
    pattern_dress[(pattern_dress == orig_color).all(axis = -1)] = replacement_color

    ################ GET DRESS OUTLINE #################
    # https://stackoverflow.com/a/40541336
    dress_icon = cv2.imread(os.path.join(absolute_path, "icons", "%s.jpg" % dress_cat), 1)
    ret,thresh = cv2.threshold(dress_icon, 127, 255, cv2.THRESH_BINARY)
    tmp = cv2.cvtColor(thresh, cv2.COLOR_RGB2GRAY)
    _,alpha = cv2.threshold(tmp, 127, 255, cv2.THRESH_BINARY_INV)
    b, g, r = cv2.split(thresh)
    rgba = [b,g,r, alpha]
    dress_outline = cv2.merge(rgba,4)

    ######### OVERLAY DRESS OUTLINE ON PATTERN #########
    # reduce change transparency of dress outline (only black pixels)
    # https://stackoverflow.com/a/11434617
    dress_outline[np.where((dress_outline == [0,0,0,255]).all(axis = 2))] = [0,0,0,170]

    dress_icon = blend_transparent(pattern_dress, dress_outline)
    dress_icon = cv2.cvtColor(dress_icon, cv2.COLOR_BGR2RGB)
    
    return dress_icon
