import os
import sys

from visualize import visualize
from ifa_multi import image_to_icon, csv_to_npy

# path to images directory
imgs_path = sys.argv[1]
# get all files and directories in the given path
all_files = os.listdir(imgs_path)
# select files with image extensions
# https://stackoverflow.com/a/25102099
image_extensions = ["jpg","jpeg","png"]
dress_images = [s for s in all_files if any(xs in s for xs in image_extensions)]

# create CSV file to write color info
with open(os.path.join(imgs_path, "color_info.csv"), "a") as csv:
    for dress_image in dress_images:
        img_path = os.path.join(imgs_path, dress_image)
        seg_dress_path = img_path.rsplit(".",1)[0]
        dress_segments_dir = "%s_segmented" % seg_dress_path
        image_to_icon(dress_image, img_path, dress_segments_dir, csv)

csv_to_npy('test_images/color_info.csv')
visualize()