from IPython import get_ipython
get_ipython().run_line_magic('matplotlib', 'inline')

import os
import numpy as np
import cv2
import tensorflow as tf, sys
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
import webcolors
from PIL import Image, ImageDraw

from seg_fcn import predict
from seg2icon import create_dress_patch,detect_texture,get_colors,draw_pattern,create_icon
from gender_age import estimate_gender_age

import dlib
from skimage import io
import numpy as np


absolute_path = os.path.dirname(os.path.realpath(__file__))

img_path = sys.argv[1]
img = cv2.cvtColor(cv2.imread(img_path), cv2.COLOR_BGR2RGB)
        
plt.figure()
plt.axis("off")
plt.imshow(img)
plt.show()

predict(img_path)

seg_dress_path = img_path.rsplit(".",1)[0]
dress_segments_dir = "%s_segmented" % seg_dress_path
print(("Dress segments directory:", dress_segments_dir))

dress_segments = os.listdir(dress_segments_dir)

dress_categories = ["blazer","blouse","dress","pants","shirt","shorts","skirt","t-shirt"]

for dress_segment in dress_segments:
    dress_cat, ext = dress_segment.split('.')
    if dress_cat in dress_categories:
        
        seg_dress_file = os.path.join(dress_segments_dir, "%s.png" % dress_cat)
        seg_dress = cv2.cvtColor(cv2.imread(seg_dress_file), cv2.COLOR_BGR2RGB)
        
        plt.figure()
        plt.axis("off")
        plt.imshow(seg_dress)
        plt.show()        
        
        dress_patch = create_dress_patch(dress_segments_dir, dress_cat)
        
        plt.figure()
        plt.axis("off")
        plt.imshow(dress_patch)
        plt.show()
        
        texture = detect_texture(dress_patch)
        
        colors_hex, bar, dress_color = get_colors(dress_patch)
        pattern = draw_pattern(colors_hex, dress_cat, texture)
        
        plt.figure()
        plt.axis("off")
        plt.imshow(pattern)
        plt.show()        
        
        dress_icon = cv2.cvtColor(create_icon(pattern, dress_cat), cv2.COLOR_BGR2RGB)
        
        plt.figure()
        plt.axis("off")
        plt.imshow(dress_icon)
        plt.show()           

        dress_icon = cv2.cvtColor(dress_icon, cv2.COLOR_BGR2RGB)
        cv2.imwrite(os.path.join(dress_segments_dir, "%s_icon.png" % dress_cat), dress_icon)

        print(("%s %s color %s" % (texture, dress_color, dress_cat)))


detector = dlib.get_frontal_face_detector()

img = io.imread(img_path)
# The 1 in the second argument indicates that we should upsample the image
# 1 time. This will make everything bigger and allow us to detect more
# faces.
dets = detector(img, 1)
print(("Number of faces detected: {}".format(len(dets))))
for i, d in enumerate(dets):
    print(("Detection {}: Left: {} Top: {} Right: {} Bottom: {}".format(
        i+1, d.left(), d.top(), d.right(), d.bottom())))
    predicted_gender, predicted_age = estimate_gender_age(img, d.top(), d.bottom(), d.left(), d.right())
    print((predicted_gender, predicted_age))
    # https://stackoverflow.com/a/26253377
    cv2.rectangle(img, (d.left(), d.top()), (d.right(), d.bottom()), (255,0,0),2)
    cv2.rectangle(img, (d.left(), d.top()-17), (d.left()+75, d.top()-1), (0,0,255),-1)
    cv2.putText(img,'%s, %.0f' % (predicted_gender, predicted_age), (d.left(), d.top()-5), cv2.FONT_HERSHEY_DUPLEX, 0.4,(255,255,255),1)
    
plt.figure()
plt.axis("off")
plt.imshow(img)

img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
cv2.imwrite(os.path.join(dress_segments_dir, "face_detected.png"), img)