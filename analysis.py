import os
import sys
import shutil
import json
import requests
from bs4 import BeautifulSoup
import datetime

# Text analysis
import re
from collections import Counter

from pymongo import MongoClient
from bson.json_util import dumps
from rq import get_current_job

import ifa_multi


def db_connect(mongo_url):
  client = MongoClient(mongo_url)
  db = client.ifa_db

  return db.analyses


def create_new_analysis(upload_path, analyses):
  now = datetime.datetime.now()
  analysis_id = "{:02}{:02}{:02}_{:02}{:02}{:02}".format(now.year, now.month, now.day,
                                                                   now.hour, now.minute, now.second)
  analysis_path = os.path.join(upload_path, analysis_id)
  os.mkdir(analysis_path)
  analysis = {'id': analysis_id, 'status': 'running', 'progress': 0, 'timestamp': str(now)}
  query_result = analyses.insert_one(analysis)
  print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
  print("CD / ANALYSIS CREATED")
  print("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")

  return analysis_id, analysis_path


def update_analysis(analysis_id, analyses):
  db_query = {"id": analysis_id}
  status_update = {"$set": {'status': 'completed'}}
  query_result = analyses.update_one(db_query, status_update)

  return query_result.acknowledged


def update_progress(analysis_id, analyses, progress):
  db_query = {"id": analysis_id}
  progress_update = {"$set": {'progress': progress}}
  query_result = analyses.update_one(db_query, progress_update)

  return query_result.acknowledged


def download_instagram_images(instagram_url, analysis_path, posts_count):
  edges = []
  page_request = requests.get(instagram_url)
  data = page_request.text
  soup = BeautifulSoup(data)
  # Get relevant script tag content
  json_content = soup.find_all("script")[3].contents[0].replace("window._sharedData = ", "").replace(";", "")
  # Parse JSON content and get posts
  json_data = json.loads(str(json_content))
  edges = json_data['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges']
  # To download first 12 images (Instagram default) use 'len(edges)' as upper limit
  for i in range(0, posts_count):
    image_url = edges[i]['node']['display_url']
    image_file_name = os.path.basename(image_url)

    image = requests.get(image_url)
    with open(os.path.join(analysis_path, image_file_name), "wb") as f:
      f.write(image.content)

  all_files = os.listdir(analysis_path)
  # Select files with image extensions (https://stackoverflow.com/a/25102099)
  image_extensions = ["jpg","jpeg","png"]
  dress_images = [s for s in all_files if any(xs in s for xs in image_extensions)]

  print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
  print("CD / EDGES")
  print(edges)
  print("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")

  return dress_images, edges


def get_post_comments(edge):
  comments_count = 0
  comments = []

  comments_count = int(edge['node']['edge_media_to_comment']['count'])
  if comments_count != 0:
    media_id = int(edge['node']['id'])
    # Generate post URL from ID
    post_url = ifa_multi.get_instagram_url_from_id(media_id)
    # Request and get post JSON
    post_request = requests.get(post_url + "?__a=1")
    print(post_request.text)
    post_json = json.loads(str(post_request.text).
                     replace("<html><body><p>", "").
                     replace("</p></body></html>", ""))
    print("----------------------")
    comment_edges = post_json['graphql']['shortcode_media']['edge_media_to_comment']['edges']
    
    for comment_edge in comment_edges:
      comment = comment_edge['node']['text']
      comments.append(comment)

  return comments, comments_count


def get_post_caption(edge):
  caption_string = ""

  caption_edge = edge['node']['edge_media_to_caption']['edges']
  if len(caption_edge) != 0:
    caption = caption_edge[0]['node']['text'].encode('utf8')
    caption_string += str(caption).replace('\n', ' ')

  return caption_string


def clean_caption_string(caption_string):
  if caption_string != "":
    # Convert all text to lower case and remove hashtags
    caption_string = caption_string.lower().replace("#", "")
    # Replace extra spaces with exactly one space
    caption_string = re.sub(' +', ' ', caption_string)
    # Remove URLs
    caption_string = re.sub(r'(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w\.-]*)*\/?\S',
                       '', caption_string, flags=re.MULTILINE)
    # Remove symbols and numbers
    caption_string = re.sub(r'[^A-Za-z]+', r' ', caption_string)
    # Remove single character words
    caption_string = ' '.join([w for w in caption_string.split() if len(w)>1])

  return caption_string


def remove_stop_words(caption_string, stop_words_file):
  stop_words = []
  with open(stop_words_file) as f:
    stop_words = f.read().splitlines()
  caption_string = ' '.join(i for i in caption_string.split() if i not in stop_words)

  return caption_string


def get_word_frequencies(caption_string):
  word_freq = Counter(caption_string.split())
  word_freq_list = []
  for key, value in word_freq.items():
    word = {}
    word["text"] = key
    word["weight"] = value
    word_freq_list.append(word)
  
  return json.dumps(word_freq_list)


def run_analysis(instagram_url, mongo_url, posts_count, upload_path, stop_words_path, json_path):
  job = get_current_job()
  # Download images from Instagram if the URL is given
  if instagram_url != '':
    # Connect to database and get collection instance
    analyses = db_connect(mongo_url)
    # Create new analysis
    analysis_id, analysis_path = create_new_analysis(upload_path, analyses)
    # Download images from Instagram
    dress_images, edges = download_instagram_images(instagram_url, analysis_path, posts_count)

    color_info_json = []
    # Create CSV file to write color info    
    with open(os.path.join(analysis_path, "color_info.csv"), "a") as csv:
      for index, dress_image in enumerate(dress_images):
        # Extract no. of comments and likes of each post for Instagram
        timestamp = 0
        likes_count = 0
        for edge in edges:
          dress_image_filename = os.path.splitext(dress_image)[0]
          # Find JSON node for current image from Instagram JSON
          if dress_image_filename in edge['node']['display_url']:
            # Extract timestamp, likes count, comments and captions as of the post as available
            timestamp = edge['node']['taken_at_timestamp']
            likes_count = edge['node']['edge_liked_by']['count']
            caption_string = get_post_caption(edge)
            comments, comments_count = get_post_comments(edge)
            break

        img_path = os.path.join(analysis_path, dress_image)
        seg_dress_path = img_path.rsplit(".",1)[0]
        dress_segments_dir = "%s_segmented" % seg_dress_path
        image_json = ifa_multi.image_to_icon(dress_image, img_path, dress_segments_dir, csv,
                                                 timestamp, likes_count, comments_count, comments)
        color_info_json.append(image_json)

        # Save progress while running
        progress = 100.0 * index / len(dress_images)
        #job.meta['progress'] = progress
        #job.save_meta()
        progress_result = update_progress(analysis_id, analyses, progress)

    # Clean caption string
    caption_string = clean_caption_string(caption_string)
    # Remove stop words from caption string
    caption_string = remove_stop_words(caption_string, stop_words_path)
    # Get word frequencies
    word_freq = get_word_frequencies(caption_string)

  agg_sentiment_json = ifa_multi.aggregate_sentiment(color_info_json)

  result_json = {
                  "analysis_id": analysis_id,
                  "source": "Instagram",
                  "posts_count": posts_count,
                  "value": instagram_url,
                  "color_info": color_info_json,
                  "agg_sentiment": agg_sentiment_json,
                  "word_freq": word_freq
                }
                
  with open(os.path.join(analysis_path, "report.json"), 'w') as result_file:
    json.dump(result_json, result_file)

  # Update completed progress
  #job.meta['progress'] = 100
  #job.save_meta()
  progress_result = update_progress(analysis_id, analyses, 100)
  # Update analysis status to completed
  status_result = update_analysis(analysis_id, analyses)

  return status_result


def get_running(mongo_url, running_analyses_path):
  analyses = db_connect(mongo_url)
  running_analyses = analyses.find({"status": "running"})

  # Remove previous JSON if exists
  if os.path.exists(running_analyses_path):
    os.remove(running_analyses_path)

  with open(running_analyses_path, 'w') as file:
    file.write(dumps(running_analyses))

  return dumps(running_analyses)


def get_completed(mongo_url, completed_analyses_path):
  analyses = db_connect(mongo_url)
  completed_analyses = analyses.find({"status": "completed"})

  # Remove previous JSON if exists
  if os.path.exists(completed_analyses_path):
    os.remove(completed_analyses_path)

  with open(completed_analyses_path, 'w') as file:
    file.write(dumps(completed_analyses))

  return dumps(completed_analyses)
