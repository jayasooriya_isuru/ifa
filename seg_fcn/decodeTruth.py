import tensorflow as tf
import numpy as np
import scipy.io
import matplotlib.pyplot as plt
import PIL
import io

data = scipy.io.loadmat("./fashionista_v0.2.1.mat",squeeze_me=True, struct_as_record=False)
jpg_bytes = data["truths"][0].image.astype('uint8').tostring()
#jpg_bytes = data["truths"][0]["image"][620].astype('uint8').tostring()
img = tf.image.decode_jpeg(jpg_bytes)

picture_stream = io.BytesIO(jpg_bytes)
picture = PIL.Image.open(picture_stream)

picture.show()
# plt.ion()
# init = tf.global_variables_initializer()
# with tf.Session() as sess:
#     sess.run(init)
#     image=img.eval()
#     print(image.shape)
#     plt.imshow(image)
#     plt.waitforbuttonpress()
        
