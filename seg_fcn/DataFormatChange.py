import tensorflow as tf
import numpy as np
import scipy.io
import matplotlib.pyplot as plt
import PIL
import io


data = scipy.io.loadmat("./fashionista_v0.2.1.mat",squeeze_me=True, struct_as_record=False)
print((data["truths"][600].annotation.labels.size))
jpg_bytes = data["truths"][400].annotation.superpixel_map.astype('uint8').tostring()
img = tf.image.decode_png(jpg_bytes)

picture_stream = io.BytesIO(jpg_bytes)
picture = PIL.Image.open(picture_stream)

value = np.asarray(picture.getdata(),dtype=np.float64).reshape((picture.size[1],picture.size[0]))

print((value.shape))

formattedImage = np.zeros((600,400))



print((formattedImage.shape))

for (x,y), k in np.ndenumerate(formattedImage):
	formattedImage[x,y] = data["truths"][400].annotation.superpixel_labels[int(value[x,y]-1)]

print((np.amax(formattedImage)))

img = PIL.Image.fromarray(formattedImage)
img.show()
