"""
Code ideas from https://github.com/Newmu/dcgan and tensorflow mnist dataset reader
"""
import numpy as np
import scipy.misc as misc
import PIL
import io
import scipy.io
import sys, traceback
from . import read_MITSceneParsingData as scene_parsing

class BatchDatset:
    valid_files = []
    train_files = []
    images = []
    coparsingIMages = []
    coparsingAnnotations = []
    coparsingAnnotationsTemp = []
    valid_images = []
    valid_annotations = []
    valid_annotationsTemp = []
    annotations = []
    annotationsTemp = []
    image_options = {}
    batch_offset = 0
    batch_offset_valid = 0
    epochs_completed = 0
    epochs_completed_valid = 0

    def __init__(self, image_options={}):
        """
        Intialize a generic file reader with batching for list of files
        :param records_list: list of file records to read -
        sample record: {'image': f, 'annotation': annotation_file, 'filename': filename}
        :param image_options: A dictionary of options for modifying the output image
        Available options:
        resize = True/ False
        resize_size = #size of output image - does bilinear resize
        color=True/False
        """
        print("Initializing Batch Dataset Reader...")
        train_records, valid_records = scene_parsing.read_dataset("Data_zoo/MIT_SceneParsing/")
        self.valid_files = valid_records
        self.train_files = train_records
        self.image_options = image_options
        print(image_options)
        self._read_images()
        self._read_images_valid()

    def _read_images(self):
        data = scipy.io.loadmat("/home/conscientai/datasets/fashionista/fashionista-v0.2.1/fashionista_v0.2.1.mat",squeeze_me=True, struct_as_record=False)
        
        formattedImage = np.zeros((600,400), dtype=np.float64)
        self.__channels = False

        for i in range(0,1):
            print(i)
            anot_bytes = data["truths"][i].annotation.superpixel_map.astype('uint8').tostring()
            img_bytes = data["truths"][i].image.astype('uint8').tostring()
            anot_picture_stream = io.BytesIO(anot_bytes)
            anot_picture = PIL.Image.open(anot_picture_stream)
            
            anot_value = np.asarray(anot_picture.getdata(),dtype=np.float64).reshape((anot_picture.size[1],anot_picture.size[0]))
            for (x,y), k in np.ndenumerate(formattedImage):
                formattedImage[x,y] = data["truths"][i].annotation.superpixel_labels[int(anot_value[x,y]-1)] - 1

            self.__channels = False
            print((np.amax(formattedImage)))
            self.annotations.append(np.expand_dims(self._transform(formattedImage), axis=3))
            
            img_picture_stream = io.BytesIO(img_bytes)
            img_picture = PIL.Image.open(img_picture_stream)
            #img_picture.show()
           
            img_value = np.asarray(img_picture.getdata(),dtype=np.float64).reshape((img_picture.size[1],img_picture.size[0],3))
            self.__channels = True
            self.images.append(self._transform(img_value))

        self.coparsingIMages = np.append(np.array([self.coparsingTransform(filename['image']) for filename in self.train_files]), np.array([self.coparsingTransform(filename['image']) for filename in self.valid_files]), axis=0)
        print((self.coparsingIMages.shape))
        self.images = np.array(self.images)
        
        self.images = np.append(self.coparsingIMages, self.images,axis=0)
        self.__channels = False
        self.coparsingAnnotations = np.append(np.array(
            [np.expand_dims(self.coparsingTransform(filename['annotation']), axis=3) for filename in self.train_files]), 
                np.array(
            [np.expand_dims(self.coparsingTransform(filename['annotation']), axis=3) for filename in self.valid_files]), axis=0)
        self.coparsingAnnotationsTemp = np.copy(self.coparsingAnnotations)
        self.reformatLabelsCoparsing()
        print((self.coparsingAnnotations.shape))
        self.annotations = np.array(self.annotations)
        self.annotationsTemp = np.copy(self.annotations)
        self.reformatLabels()
        self.annotations = np.append(self.coparsingAnnotations, self.annotations,axis=0)
       # self.annotations = np.array(self.annotations)
        print((self.images.shape))
        print((self.annotations.shape))

        
        print((np.amax(self.coparsingAnnotations)))
       
        

    def reformatLabelsCoparsing(self):
        for i in range(0,59):
            if (i==1):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,0,self.coparsingAnnotationsTemp)
                print("###########")
                print(i)
            elif (i==2):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,0,self.coparsingAnnotationsTemp)
                print("###########")
                print(i)
            elif (i==3):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,0,self.coparsingAnnotations)
            elif (i==4):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,3,self.coparsingAnnotations)
            elif (i==5):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,11,self.coparsingAnnotations)
            elif (i==6):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,14,self.coparsingAnnotations)
            elif (i==7):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,6,self.coparsingAnnotations)
            elif (i==8):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,11,self.coparsingAnnotations)
            elif (i==9):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,0,self.coparsingAnnotations)
            elif (i==10):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,0,self.coparsingAnnotations)

            elif (i==11):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,0,self.coparsingAnnotations)
            elif (i==12):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,6,self.coparsingAnnotations)
            elif (i==13):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,3,self.coparsingAnnotations)
            elif (i==14):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,14,self.coparsingAnnotations)
            elif (i==15):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,0,self.coparsingAnnotations)
            elif (i==16):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,6,self.coparsingAnnotations)
            elif (i==17):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,0,self.coparsingAnnotations)
            elif (i==18):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,0,self.coparsingAnnotations)
            elif (i==19):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,54,self.coparsingAnnotations)
                
            elif (i==20):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,21,self.coparsingAnnotations)
            elif (i==21):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,6,self.coparsingAnnotations)
            elif (i==22):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,4,self.coparsingAnnotations)
            elif (i==23):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,0,self.coparsingAnnotations)
            elif (i==24):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,11,self.coparsingAnnotations)
            elif (i==25):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,15,self.coparsingAnnotations)
            elif (i==26):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,4,self.coparsingAnnotations)
            elif (i==27):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,15,self.coparsingAnnotations)
            elif (i==28):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,6,self.coparsingAnnotations)
            elif (i==29):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,0,self.coparsingAnnotations)
            elif (i==30):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,0,self.coparsingAnnotations)
            elif (i==31):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,15,self.coparsingAnnotations)
            elif (i==32):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,6,self.coparsingAnnotations)
            elif (i==33):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,0,self.coparsingAnnotations)
            elif (i==34):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,0,self.coparsingAnnotations)
            elif (i==35):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,14,self.coparsingAnnotations)
            elif (i==36):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,6,self.coparsingAnnotations)
            elif (i==37):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,0,self.coparsingAnnotations)
            elif (i==38):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,17,self.coparsingAnnotations)
            elif (i==39):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,6,self.coparsingAnnotations)
            elif (i==40):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,2,self.coparsingAnnotations)
            elif (i==41):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,55,self.coparsingAnnotations)
            elif (i==42):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,8,self.coparsingAnnotations)
            elif (i==43):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,6,self.coparsingAnnotations)
            elif (i==44):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,6,self.coparsingAnnotations)
            elif (i==45):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,6,self.coparsingAnnotations)
            elif (i==46):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,36,self.coparsingAnnotations)
            elif (i==47):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,0,self.coparsingAnnotations)
            elif (i==48):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,4,self.coparsingAnnotations)
            elif (i==49):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,4,self.coparsingAnnotations)
            elif (i==50):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,14,self.coparsingAnnotations)
            elif (i==51):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,4,self.coparsingAnnotations)
            elif (i==52):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,0,self.coparsingAnnotations)
            elif (i==53):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,2,self.coparsingAnnotations)
            elif (i==54):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,11,self.coparsingAnnotations)
            elif (i==55):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,4,self.coparsingAnnotations)
            elif (i==56):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,0,self.coparsingAnnotations)
            elif (i==57):
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,0,self.coparsingAnnotations)
            elif (i==58):
                print("###########")
                print((np.amax(self.coparsingAnnotations)))
                self.coparsingAnnotations = np.where(self.coparsingAnnotationsTemp==i,0,self.coparsingAnnotations)
                print((np.amax(self.coparsingAnnotations)))

    def coparsingTransform(self, filename):
        image = misc.imread(filename)
        if self.__channels and len(image.shape) < 3:  # make sure images are of shape(h,w,3)
            image = np.array([image for i in range(3)])

        if self.image_options.get("resize", False) and self.image_options["resize"]:
            resize_size = int(self.image_options["resize_size"])
            resize_image = misc.imresize(image,
                                         [resize_size, resize_size], interp='nearest')
        else:
            resize_image = image

        return np.array(resize_image)        


    def reformatLabels(self):
        for i in range(0,56):
            if (i==1):
                self.annotations = np.where(self.annotationsTemp==i,2,self.annotations)
            elif (i==5):
                self.annotations = np.where(self.annotationsTemp==i,0,self.annotations)
            elif (i==7):
                self.annotations = np.where(self.annotationsTemp==i,3,self.annotations)
            elif (i==9):
                self.annotations = np.where(self.annotationsTemp==i,0,self.annotations)
            elif (i==10):
                self.annotations = np.where(self.annotationsTemp==i,6,self.annotations)
            elif (i==12):
                self.annotations = np.where(self.annotationsTemp==i,11,self.annotations)
            elif (i==13):
                self.annotations = np.where(self.annotationsTemp==i,11,self.annotations)
            elif (i==16):
                self.annotations = np.where(self.annotationsTemp==i,4,self.annotations)
            elif (i==18):
                self.annotations = np.where(self.annotationsTemp==i,15,self.annotations)
            elif (i==19):
                self.annotations = np.where(self.annotationsTemp==i,15,self.annotations)
            elif (i==20):
                self.annotations = np.where(self.annotationsTemp==i,0,self.annotations)
            elif (i==22):
                self.annotations = np.where(self.annotationsTemp==i,11,self.annotations)
            elif (i==23):
                self.annotations = np.where(self.annotationsTemp==i,0,self.annotations)
            elif (i==24):
                self.annotations = np.where(self.annotationsTemp==i,0,self.annotations)
            elif (i==25):
                self.annotations = np.where(self.annotationsTemp==i,4,self.annotations)
            elif (i==26):
                self.annotations = np.where(self.annotationsTemp==i,0,self.annotations)
            elif (i==27):
                self.annotations = np.where(self.annotationsTemp==i,0,self.annotations)
            elif (i==28):
                self.annotations = np.where(self.annotationsTemp==i,6,self.annotations)
            elif (i==29):
                self.annotations = np.where(self.annotationsTemp==i,0,self.annotations)
            elif (i==30):
                self.annotations = np.where(self.annotationsTemp==i,0,self.annotations)
            elif (i==31):
                self.annotations = np.where(self.annotationsTemp==i,6,self.annotations)
            elif (i==32):
                self.annotations = np.where(self.annotationsTemp==i,0,self.annotations)
            elif (i==33):
                self.annotations = np.where(self.annotationsTemp==i,0,self.annotations)
            elif (i==34):
                self.annotations = np.where(self.annotationsTemp==i,4,self.annotations)
            elif (i==35):
                self.annotations = np.where(self.annotationsTemp==i,4,self.annotations)
            elif (i==37):
                self.annotations = np.where(self.annotationsTemp==i,0,self.annotations)
            elif (i==38):
                self.annotations = np.where(self.annotationsTemp==i,6,self.annotations)
            elif (i==39):
                self.annotations = np.where(self.annotationsTemp==i,0,self.annotations)
            elif (i==40):
                self.annotations = np.where(self.annotationsTemp==i,0,self.annotations)
            elif (i==41):
                self.annotations = np.where(self.annotationsTemp==i,6,self.annotations)
            elif (i==42):
                self.annotations = np.where(self.annotationsTemp==i,0,self.annotations)
            elif (i==43):
                self.annotations = np.where(self.annotationsTemp==i,14,self.annotations)
            elif (i==44):
                self.annotations = np.where(self.annotationsTemp==i,6,self.annotations)
            elif (i==45):
                self.annotations = np.where(self.annotationsTemp==i,0,self.annotations)
            elif (i==46):
                self.annotations = np.where(self.annotationsTemp==i,0,self.annotations)
            elif (i==47):
                self.annotations = np.where(self.annotationsTemp==i,6,self.annotations)
            elif (i==48):
                self.annotations = np.where(self.annotationsTemp==i,6,self.annotations)
            elif (i==49):
                self.annotations = np.where(self.annotationsTemp==i,0,self.annotations)
            elif (i==50):
                self.annotations = np.where(self.annotationsTemp==i,6,self.annotations)
            elif (i==51):
                self.annotations = np.where(self.annotationsTemp==i,0,self.annotations)
            elif (i==52):
                self.annotations = np.where(self.annotationsTemp==i,14,self.annotations)
            elif (i==53):
                self.annotations = np.where(self.annotationsTemp==i,6,self.annotations)


    def _read_images_valid(self):
        data = scipy.io.loadmat("/home/conscientai/datasets/fashionista/fashionista-v0.2.1/fashionista_v0.2.1.mat",squeeze_me=True, struct_as_record=False)
        
        formattedImage = np.zeros((600,400), dtype=np.float64)
        self.__channels = False
      
        for i in range(600,602):
            print(i)
            anot_bytes = data["truths"][i].annotation.superpixel_map.astype('uint8').tostring()
            img_bytes = data["truths"][i].image.astype('uint8').tostring()
            anot_picture_stream = io.BytesIO(anot_bytes)
            anot_picture = PIL.Image.open(anot_picture_stream)
            
            anot_value = np.asarray(anot_picture.getdata(),dtype=np.float64).reshape((anot_picture.size[1],anot_picture.size[0]))
            for (x,y), k in np.ndenumerate(formattedImage):
                formattedImage[x,y] = data["truths"][i].annotation.superpixel_labels[int(anot_value[x,y]-1)] - 1

            self.__channels = False
            print((np.amax(formattedImage)))
            self.valid_annotations.append(np.expand_dims(self._transform(formattedImage), axis=3))
            
            img_picture_stream = io.BytesIO(img_bytes)
            img_picture = PIL.Image.open(img_picture_stream)
            #img_picture.show
           
            img_value = np.asarray(img_picture.getdata(),dtype=np.float64).reshape((img_picture.size[1],img_picture.size[0],3))
            self.__channels = True
            self.valid_images.append(self._transform(img_value))

        self.valid_images = np.array(self.valid_images)
        self.valid_annotations = np.array(self.valid_annotations)
        self.valid_annotationsTemp = np.copy(self.valid_annotations)
        self.reformatLabelsValid()
        print((np.amin(self.valid_annotations)))
        print((self.valid_images.shape))
        print((self.valid_annotations.shape))

        
    def reformatLabelsValid(self):
        for i in range(0,56):
            if (i==1):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,2,self.valid_annotations)
            elif (i==5):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,0,self.valid_annotations)
            elif (i==7):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,3,self.valid_annotations)
            elif (i==9):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,0,self.valid_annotations)
            elif (i==10):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,6,self.valid_annotations)
            elif (i==12):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,11,self.valid_annotations)
            elif (i==13):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,11,self.valid_annotations)
            elif (i==16):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,4,self.valid_annotations)
            elif (i==18):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,15,self.valid_annotations)
            elif (i==19):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,15,self.valid_annotations)
            elif (i==20):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,0,self.valid_annotations)
            elif (i==22):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,11,self.valid_annotations)
            elif (i==23):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,0,self.valid_annotations)
            elif (i==24):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,0,self.valid_annotations)
            elif (i==25):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,4,self.valid_annotations)
            elif (i==26):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,0,self.valid_annotations)
            elif (i==27):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,0,self.valid_annotations)
            elif (i==28):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,6,self.valid_annotations)
            elif (i==29):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,0,self.valid_annotations)
            elif (i==30):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,0,self.valid_annotations)
            elif (i==31):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,6,self.valid_annotations)
            elif (i==32):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,0,self.valid_annotations)
            elif (i==33):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,0,self.valid_annotations)
            elif (i==34):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,4,self.valid_annotations)
            elif (i==35):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,4,self.valid_annotations)
            elif (i==37):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,0,self.valid_annotations)
            elif (i==38):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,6,self.valid_annotations)
            elif (i==39):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,0,self.valid_annotations)
            elif (i==40):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,0,self.valid_annotations)
            elif (i==41):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,6,self.valid_annotations)
            elif (i==42):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,0,self.valid_annotations)
            elif (i==43):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,14,self.valid_annotations)
            elif (i==44):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,6,self.valid_annotations)
            elif (i==45):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,0,self.valid_annotations)
            elif (i==46):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,0,self.valid_annotations)
            elif (i==47):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,6,self.valid_annotations)
            elif (i==48):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,6,self.valid_annotations)
            elif (i==49):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,0,self.valid_annotations)
            elif (i==50):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,6,self.valid_annotations)
            elif (i==51):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,0,self.valid_annotations)
            elif (i==52):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,14,self.valid_annotations)
            elif (i==53):
                self.valid_annotations = np.where(self.valid_annotationsTemp==i,6,self.valid_annotations)


    def _transform(self, formattedImage):
        
        if self.__channels and len(formattedImage.shape) < 3:  # make sure images are of shape(h,w,3)
            formattedImage = np.array([formattedImage for i in range(3)])

        if self.image_options.get("resize", False) and self.image_options["resize"] and not self.__channels:
            resize_size = int(self.image_options["resize_size"])
            
            resize_image = misc.imresize(formattedImage,
                                         [resize_size, resize_size], interp='nearest', mode='F')
            
        elif self.image_options.get("resize", False) and self.image_options["resize"] and self.__channels:
            resize_size = int(self.image_options["resize_size"])
            
            resize_image = misc.imresize(formattedImage,
                                         [resize_size, resize_size], interp='nearest')
            
        else:
            resize_image = formattedImage

        
        # if not self.__channels:
        #     img = PIL.Image.fromarray(resize_image)
        #     img.show()
        return np.array(resize_image)

    def get_records(self):
        return self.images, self.annotations

    def reset_batch_offset(self, offset=0):
        self.batch_offset = offset

    def next_batch(self, batch_size):
        start = self.batch_offset
        self.batch_offset += batch_size
        if self.batch_offset > self.images.shape[0]:
            #Finished epoch
            # self.epochs_completed += 1
            # print("****************** Epochs completed: " + str(self.epochs_completed) + "******************")
            # # Shuffle the data
            # perm = np.arange(self.images.shape[0])
            # np.random.shuffle(perm)
            # self.images = self.images[perm]
            # self.annotations = self.annotations[perm]
            # # Start next epoch
            # start = 0
            # self.batch_offset = batch_size
            print("Finished")
            exit()

        end = self.batch_offset
        return self.images[start:end], self.annotations[start:end]

    def next_batch_valid(self, batch_size):
        start = self.batch_offset_valid
        self.batch_offset_valid += batch_size
        if self.batch_offset_valid > self.valid_images.shape[0]:
            # Finished epoch
            self.epochs_completed_valid += 1
            print(("****************** Epochs completed: " + str(self.epochs_completed_valid) + "******************"))
            # Shuffle the data
            perm = np.arange(self.valid_images.shape[0])
            np.random.shuffle(perm)
            self.valid_images = self.valid_images[perm]
            self.valid_annotations = self.valid_annotations[perm]
            # Start next epoch
            start = 0
            self.batch_offset_valid = batch_size

        end = self.batch_offset_valid
        return self.valid_images[start:end], self.valid_annotations[start:end]

    def get_random_batch(self, batch_size):
        indexes = np.random.randint(0, self.images.shape[0], size=[batch_size]).tolist()
        return self.images[indexes], self.annotations[indexes]
