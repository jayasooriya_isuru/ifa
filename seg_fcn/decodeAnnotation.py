import tensorflow as tf
import numpy as np
import scipy.io
import matplotlib.pyplot as plt
import PIL
import io


data = scipy.io.loadmat("./fashionista_v0.2.1.mat",squeeze_me=True, struct_as_record=False)
print((data["truths"][10].annotation.superpixel_map))
jpg_bytes = data["truths"][620].annotation.superpixel_map.astype('uint8').tostring()
img = tf.image.decode_png(jpg_bytes)

picture_stream = io.BytesIO(jpg_bytes)
picture = PIL.Image.open(picture_stream)
picture.show()

value = np.asarray(picture.getdata(),dtype=np.float64).reshape((picture.size[1],picture.size[0]))
np.savetxt("img_pixels.csv", value, delimiter=',')
print((np.amax(value)))
print((data["truths"][620].annotation.superpixel_labels.shape))

# print whether JPEG, PNG, etc.
print((picture.format))

plt.ion()
init = tf.global_variables_initializer()
with tf.Session() as sess:
    sess.run(init)
    image=img.eval()
    print((image.shape))
    plt.imshow(image.squeeze())
    plt.waitforbuttonpress()
        
