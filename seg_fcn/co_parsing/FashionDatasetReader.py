"""
Code ideas from https://github.com/Newmu/dcgan and tensorflow mnist dataset reader
"""
import numpy as np
import scipy.misc as misc
import PIL
import io
import scipy.io
import sys, traceback

class BatchDatset:
    files = []
    images = []
    valid_images = []
    valid_annotations = []
    annotations = []
    image_options = {}
    batch_offset = 0
    batch_offset_valid = 0
    epochs_completed = 0
    epochs_completed_valid = 0

    def __init__(self, image_options={}):
        """
        Intialize a generic file reader with batching for list of files
        :param records_list: list of file records to read -
        sample record: {'image': f, 'annotation': annotation_file, 'filename': filename}
        :param image_options: A dictionary of options for modifying the output image
        Available options:
        resize = True/ False
        resize_size = #size of output image - does bilinear resize
        color=True/False
        """
        print("Initializing Batch Dataset Reader...")
        self.image_options = image_options
        print(image_options)
        self._read_images()
        self._read_images_valid()

    def _read_images(self):
        data = scipy.io.loadmat("/home/conscientai/datasets/fashionista/fashionista-v0.2.1/fashionista_v0.2.1.mat",squeeze_me=True, struct_as_record=False)
        
        formattedImage = np.zeros((600,400), dtype=np.float64)
        self.__channels = False
      
        for i in range(0,2):
            print(i)
            anot_bytes = data["truths"][i].annotation.superpixel_map.astype('uint8').tostring()
            img_bytes = data["truths"][i].image.astype('uint8').tostring()
            anot_picture_stream = io.BytesIO(anot_bytes)
            anot_picture = PIL.Image.open(anot_picture_stream)
            
            anot_value = np.asarray(anot_picture.getdata(),dtype=np.float64).reshape((anot_picture.size[1],anot_picture.size[0]))
            for (x,y), k in np.ndenumerate(formattedImage):
                formattedImage[x,y] = data["truths"][i].annotation.superpixel_labels[int(anot_value[x,y]-1)] - 1

            self.__channels = False
            print((np.amax(formattedImage)))
            self.annotations.append(np.expand_dims(self._transform(formattedImage), axis=3))
            
            img_picture_stream = io.BytesIO(img_bytes)
            img_picture = PIL.Image.open(img_picture_stream)
            #img_picture.show()
           
            img_value = np.asarray(img_picture.getdata(),dtype=np.float64).reshape((img_picture.size[1],img_picture.size[0],3))
            self.__channels = True
            self.images.append(self._transform(img_value))

        self.images = np.array(self.images)
        self.annotations = np.array(self.annotations)
        print((np.amin(self.annotations)))
        print((self.images.shape))
        print((self.annotations.shape))


    def _read_images_valid(self):
        data = scipy.io.loadmat("/home/conscientai/datasets/fashionista/fashionista-v0.2.1/fashionista_v0.2.1.mat",squeeze_me=True, struct_as_record=False)
        
        formattedImage = np.zeros((600,400), dtype=np.float64)
        self.__channels = False
      
        for i in range(600,602):
            print(i)
            anot_bytes = data["truths"][i].annotation.superpixel_map.astype('uint8').tostring()
            img_bytes = data["truths"][i].image.astype('uint8').tostring()
            anot_picture_stream = io.BytesIO(anot_bytes)
            anot_picture = PIL.Image.open(anot_picture_stream)
            
            anot_value = np.asarray(anot_picture.getdata(),dtype=np.float64).reshape((anot_picture.size[1],anot_picture.size[0]))
            for (x,y), k in np.ndenumerate(formattedImage):
                formattedImage[x,y] = data["truths"][i].annotation.superpixel_labels[int(anot_value[x,y]-1)] - 1

            self.__channels = False
            print((np.amax(formattedImage)))
            self.valid_annotations.append(np.expand_dims(self._transform(formattedImage), axis=3))
            
            img_picture_stream = io.BytesIO(img_bytes)
            img_picture = PIL.Image.open(img_picture_stream)
            #img_picture.show()
           
            img_value = np.asarray(img_picture.getdata(),dtype=np.float64).reshape((img_picture.size[1],img_picture.size[0],3))
            self.__channels = True
            self.valid_images.append(self._transform(img_value))

        self.valid_images = np.array(self.valid_images)
        self.valid_annotations = np.array(self.valid_annotations)
        print((np.amin(self.valid_annotations)))
        print((self.valid_images.shape))
        print((self.valid_annotations.shape))

        

    def _transform(self, formattedImage):
        
        if self.__channels and len(formattedImage.shape) < 3:  # make sure images are of shape(h,w,3)
            formattedImage = np.array([formattedImage for i in range(3)])

        if self.image_options.get("resize", False) and self.image_options["resize"] and not self.__channels:
            resize_size = int(self.image_options["resize_size"])
            
            resize_image = misc.imresize(formattedImage,
                                         [resize_size, resize_size], interp='nearest', mode='F')
            
        elif self.image_options.get("resize", False) and self.image_options["resize"] and self.__channels:
            resize_size = int(self.image_options["resize_size"])
            
            resize_image = misc.imresize(formattedImage,
                                         [resize_size, resize_size], interp='nearest')
            
        else:
            resize_image = formattedImage

        
        # if not self.__channels:
        #     img = PIL.Image.fromarray(resize_image)
        #     img.show()
        return np.array(resize_image)

    def get_records(self):
        return self.images, self.annotations

    def reset_batch_offset(self, offset=0):
        self.batch_offset = offset

    def next_batch(self, batch_size):
        start = self.batch_offset
        self.batch_offset += batch_size
        if self.batch_offset > self.images.shape[0]:
            # Finished epoch
            self.epochs_completed += 1
            print(("****************** Epochs completed: " + str(self.epochs_completed) + "******************"))
            # Shuffle the data
            perm = np.arange(self.images.shape[0])
            np.random.shuffle(perm)
            self.images = self.images[perm]
            self.annotations = self.annotations[perm]
            # Start next epoch
            start = 0
            self.batch_offset = batch_size

        end = self.batch_offset
        return self.images[start:end], self.annotations[start:end]

    def next_batch_valid(self, batch_size):
        start = self.batch_offset_valid
        self.batch_offset_valid += batch_size
        if self.batch_offset_valid > self.valid_images.shape[0]:
            # Finished epoch
            self.epochs_completed_valid += 1
            print(("****************** Epochs completed: " + str(self.epochs_completed_valid) + "******************"))
            # Shuffle the data
            perm = np.arange(self.valid_images.shape[0])
            np.random.shuffle(perm)
            self.valid_images = self.valid_images[perm]
            self.valid_annotations = self.valid_annotations[perm]
            # Start next epoch
            start = 0
            self.batch_offset_valid = batch_size

        end = self.batch_offset_valid
        return self.valid_images[start:end], self.valid_annotations[start:end]

    def get_random_batch(self, batch_size):
        indexes = np.random.randint(0, self.images.shape[0], size=[batch_size]).tolist()
        return self.images[indexes], self.annotations[indexes]
