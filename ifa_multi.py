# https://matplotlib.org/examples/color/named_colors.html


import os
import numpy as np
import cv2
import tensorflow as tf, sys
import webcolors

from seg_fcn import predict
from seg2icon import create_dress_patch,detect_texture,get_colors,draw_pattern,create_icon
from gender_age import estimate_gender_age
from visualize import visualize

import dlib
from skimage import io

from colormath.color_objects import LabColor, sRGBColor
from colormath.color_conversions import convert_color
from scipy import spatial

import nltk
from nltk.sentiment.vader import SentimentIntensityAnalyzer


absolute_path = os.path.dirname(os.path.realpath(__file__))

def image_to_icon(dress_image, img_path, dress_segments_dir, csv, timestamp, likes_count, comments_count, comments):
    img = cv2.cvtColor(cv2.imread(img_path), cv2.COLOR_BGR2RGB)

    predict(img_path)
    print(("Dress segments directory:", dress_segments_dir))
    dress_segments = os.listdir(dress_segments_dir)

    #dress_categories = ["blazer","blouse","dress","pants","shirt","shorts","skirt","t-shirt"]
    dress_categories = ["blouse","dress","pants","shirt","shorts","skirt","t-shirt"]

    comments_sentiment = {}
    if comments_count > 0:
      comments_sentiment = analyse_sentiment(comments)

    image_json = {
                    "image": dress_image,
                    "timestamp": timestamp,
                    "comments_count": comments_count,
                    "comments_sentiment": comments_sentiment,
                    "likes_count": likes_count,
                    "dress_items": []
                 }

    for dress_segment in dress_segments:
        dress_cat, ext = dress_segment.split('.')
        if dress_cat in dress_categories:
            
            seg_dress_file = os.path.join(dress_segments_dir, "%s.png" % dress_cat)
            seg_dress_file_bar = os.path.join(dress_segments_dir, "%s_bar" % dress_cat)
            mask_file = os.path.join(dress_segments_dir, "%s_mask.txt" % dress_cat)
            seg_dress = cv2.cvtColor(cv2.imread(seg_dress_file), cv2.COLOR_BGR2RGB)

            mask = np.loadtxt(mask_file, dtype=int)
            colors_hex, color_ratios, bar, dress_color = get_colors(mask, True, seg_dress_file_bar)
            # write color info in CSV format
            csv.write("%s,%s,%s,%s,%s,%s,%s,%s\n" % (dress_image, dress_cat, colors_hex[0].upper(), color_ratios[0],
                colors_hex[1].upper(), color_ratios[1], colors_hex[2].upper(), color_ratios[2]))
            
            # Comment below only for color analysis results.
            # dress_patch = create_dress_patch(dress_segments_dir, dress_cat)
            # texture = detect_texture(dress_patch)
            # pattern = draw_pattern(colors_hex, dress_cat, texture)
            # dress_icon = cv2.cvtColor(create_icon(pattern, dress_cat), cv2.COLOR_BGR2RGB)
            # dress_icon = cv2.cvtColor(dress_icon, cv2.COLOR_BGR2RGB)
            # cv2.imwrite(os.path.join(dress_segments_dir, "%s_icon.png" % dress_cat), dress_icon)
            # print("%s %s color %s" % (texture, dress_color, dress_cat))

            dress_item_json = {
                                  "dress_item":dress_cat,
                                  "div":"#img-" + dress_image.replace(".jpg","") + "-" + dress_cat,
                                  "chart_data":[
                                      {
                                          "key":colors_hex[0].upper(),
                                          "y":color_ratios[0],
                                          "color":colors_hex[0].upper()
                                      },
                                      {
                                          "key":colors_hex[1].upper(),
                                          "y":color_ratios[1],
                                          "color":colors_hex[1].upper()
                                      },
                                      {
                                          "key":colors_hex[2].upper(),
                                          "y":color_ratios[2],
                                          "color":colors_hex[2].upper()
                                      }
                                  ]
                              }

            image_json["dress_items"].append(dress_item_json)

    return image_json

def estimate_age_gender(img_path, dress_segments_dir):
    detector = dlib.get_frontal_face_detector()

    img = io.imread(img_path)
    # The 1 in the second argument indicates that we should upsample the image
    # 1 time. This will make everything bigger and allow us to detect more
    # faces.
    dets = detector(img, 1)
    print(("Number of faces detected: {}".format(len(dets))))
    for i, d in enumerate(dets):
        print(("Detection {}: Left: {} Top: {} Right: {} Bottom: {}".format(
            i+1, d.left(), d.top(), d.right(), d.bottom())))
        predicted_gender, predicted_age = estimate_gender_age(img, d.top(), d.bottom(), d.left(), d.right())
        print((predicted_gender, predicted_age))
        # https://stackoverflow.com/a/26253377
        cv2.rectangle(img, (d.left(), d.top()), (d.right(), d.bottom()), (255,0,0),2)
        cv2.rectangle(img, (d.left(), d.top()-17), (d.left()+75, d.top()-1), (0,0,255),-1)
        cv2.putText(img,'%s, %.0f' % (predicted_gender, predicted_age), (d.left(), d.top()-5), cv2.FONT_HERSHEY_DUPLEX, 0.4,(255,255,255),1)

    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    cv2.imwrite(os.path.join(dress_segments_dir, "face_detected.png"), img)


def hex_to_rgb(hex):
    return tuple(int(hex[i:i+2], 16) for i in (0, 2 ,4))


def rgb_to_lab(rgb_colors):
    RGB = sRGBColor(rgb_colors[0],rgb_colors[1], rgb_colors[2], True)
    return convert_color(RGB, LabColor).get_value_tuple()


def find_nearest(array, value):
    tree = spatial.cKDTree(array)
    min_dist, min_id = tree.query(value)
    return min_id


def csv_to_npy(csv_path):
    color_data = np.genfromtxt(csv_path, delimiter=',', dtype=str, comments='/')

    row_num = color_data.shape[0]
    column_num = color_data.shape[1]

    hex_color_map = ['#FFCCCC','#FFDBC8','#FFECC8','#FFFDC8','#EEFDC8','#D4FDC8','#CCFDD1','#CCFDE2','#CCFDF2','#CCF4FB','#CCECFB',
                     '#CCDBFB','#CCCAFB','#DDCAFB','#EECAFB','#FFCAFB','#FFCAEA','#FFCAD9','#FFCAD0','#FF9999','#FFB996','#FFDB96',
                     '#FFFD96','#DDFD96','#AAFD96','#99FDA7','#99FDC9','#99FDEB','#99ECFC','#99DBFC','#99B9FC','#9997FC','#BB97FC',
                     '#DD97FC','#FF97FC','#FF97DA','#FF97B8','#FF97A7','#FF6666','#FF9864','#FFCB64','#FFFE64','#CCFE64','#7FFE64',
                     '#66FE7E','#66FEB1','#66FEE3','#66E4FD','#66CBFD','#6698FD','#6665FD','#9965FD','#CC65FD','#FF65FD','#FF65CA',
                     '#FF6597','#FF657D','#FF3333','#FF7632','#FFBA32','#FFFE32','#BBFE32','#55FE32','#33FE54','#33FE98','#33FEDC',
                     '#33DCFE','#33BAFE','#3376FE','#3332FE','#7732FE','#BB32FE','#FF32FE','#FF32BA','#FF3276','#FF3254','#FF0000',
                     '#FF5500','#FFAA00','#FFFF00','#AAFF00','#2AFF00','#00FF2B','#00FF80','#00FFD4','#00D4FF','#00AAFF','#0055FF',
                     '#0000FF','#5500FF','#AA00FF','#FF00FF','#FF00AA','#FF0055','#FF002A','#CC0000','#CC4400','#CC8800','#CCCC00',
                     '#88CC00','#22CC00','#00CC22','#00CC66','#00CCAA','#00AACC','#0088CC','#0044CC','#0000CC','#4400CC','#8800CC',
                     '#CC00CC','#CC0088','#CC0044','#CC0022','#990000','#993300','#996600','#999900','#669900','#199900','#00991A',
                     '#00994D','#00997F','#007F99','#006699','#003399','#000099','#330099','#660099','#990099','#990066','#990033',
                     '#990019','#660000','#662200','#664400','#666600','#446600','#116600','#006611','#006633','#006655','#005566',
                     '#004466','#002266','#000066','#220066','#440066','#660066','#660044','#660022','#660011','#000000','#0E0E0E',
                     '#1C1C1C','#2A2A2A','#393939','#474747','#555555','#636363','#717171','#7F7F7F','#8E8E8E','#9C9C9C','#AAAAAA',
                     '#B8B8B8','#C6C6C6','#D4D4D4','#E3E3E3','#F1F1F1','#FFFFFF']

    lab_color_map = np.zeros(shape=(np.asarray(hex_color_map).size,3))
    histogram = np.zeros(shape=(1, np.asarray(hex_color_map).size))

    for j in range(np.asarray(hex_color_map).size):
        lab_color_map[j,:] = rgb_to_lab(hex_to_rgb(hex_color_map[j].lstrip('#')))

    for i in range(row_num):
        rgb_color1 = hex_to_rgb(color_data[i,2].lstrip('#'))
        rgb_color1_precentage = color_data[i,3]
        lab_color = rgb_to_lab(rgb_color1)
        nearest_color_id = find_nearest(lab_color_map,lab_color)
        histogram[0, nearest_color_id] = histogram[0, nearest_color_id] + float(rgb_color1_precentage)
        
        rgb_color2 = hex_to_rgb(color_data[i,4].lstrip('#'))
        rgb_color2_precentage = color_data[i,5]
        lab_color = rgb_to_lab(rgb_color2)
        nearest_color_id = find_nearest(lab_color_map,lab_color)
        histogram[0, nearest_color_id] = histogram[0, nearest_color_id] + float(rgb_color2_precentage)

        rgb_color3 = hex_to_rgb(color_data[i,6].lstrip('#'))
        rgb_color3_precentage = color_data[i,7]
        lab_color = rgb_to_lab(rgb_color3)
        nearest_color_id = find_nearest(lab_color_map,lab_color)
        histogram[0, nearest_color_id] = histogram[0, nearest_color_id] + float(rgb_color3_precentage)

    sum = np.sum(histogram)
    histogram = histogram/sum
    np.save('color_info.npy', histogram)
    #print(histogram)


# https://stackoverflow.com/a/45196004
def get_instagram_url_from_id(media_id):
    alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_'
    shortened_id = ''

    while media_id > 0:
        remainder = media_id % 64
        # dual conversion sign gets the right ID for new posts
        media_id = (media_id - remainder) // 64;
        # remainder should be casted as an integer to avoid a type error. 
        shortened_id = alphabet[int(remainder)] + shortened_id

    return 'https://instagram.com/p/' + shortened_id + '/'


def analyse_sentiment(comments):
  sentiment_analyzer = SentimentIntensityAnalyzer()
  pos = 0
  neg = 0
  neu = 0
  for comment in comments:
    polarity_scores = sentiment_analyzer.polarity_scores(comment)
    pos += polarity_scores['pos']
    neg += polarity_scores['neg']
    neu += polarity_scores['neu']

  sentiment_json = {}
  if len(comments) > 0:
    sentiment_json = {
                        "chart_data":[
                            {
                                "key": "positive",
                                "y": pos/len(comments),
                                "color": "#40BF80"
                            },
                            {
                                "key": "negative",
                                "y": neg/len(comments),
                                "color": "#FF6666"
                            },
                            {
                                "key": "neutral",
                                "y": neu/len(comments),
                                "color": "#CCCCCC"
                            }
                        ]
                      }
  return sentiment_json


def aggregate_sentiment(posts_json):
  agg_pos = 0
  agg_neg = 0
  agg_neu = 0
  comment_posts_count = 0
  for post_json in posts_json:
    if (post_json['comments_count'] > 0) & (post_json['comments_sentiment'] != {}):
      agg_pos += post_json['comments_sentiment']['chart_data'][0]['y']
      agg_neg += post_json['comments_sentiment']['chart_data'][1]['y']
      agg_neu += post_json['comments_sentiment']['chart_data'][2]['y']
      comment_posts_count += 1

  agg_sentiment_json = {}
  if comment_posts_count > 0:
    agg_sentiment_json = {
                           "chart_data":[
                               {
                                   "key": "positive",
                                   "y": agg_pos/comment_posts_count,
                                   "color": "#40BF80"
                               },
                               {
                                   "key": "negative",
                                   "y": agg_neg/comment_posts_count,
                                   "color": "#FF6666"
                               },
                               {
                                   "key": "neutral",
                                   "y": agg_neu/comment_posts_count,
                                   "color": "#CCCCCC"
                               }
                           ]
                         }
  return agg_sentiment_json


def main():
    # path to images directory
    imgs_path = sys.argv[1]
    # get all files and directories in the given path
    all_files = os.listdir(imgs_path)
    # select files with image extensions
    # https://stackoverflow.com/a/25102099
    image_extensions = ["jpg","jpeg","png"]
    dress_images = [s for s in all_files if any(xs in s for xs in image_extensions)]

    # create CSV file to write color info
    with open(os.path.join(imgs_path, "color_info.csv"), "a") as csv:
        for dress_image in dress_images:
            img_path = os.path.join(imgs_path, dress_image)
            seg_dress_path = img_path.rsplit(".",1)[0]
            dress_segments_dir = "%s_segmented" % seg_dress_path
            image_to_icon(dress_image, img_path, dress_segments_dir, csv)

    csv_to_npy('images/color_info.csv')

if __name__ == "__main__":
    main()